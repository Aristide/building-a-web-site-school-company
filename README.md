# Yakachoisir


This is a project realized as part of my studies at EPITA. The goal is to realize a project from start to finish in team.
The principle is to respond to a call for tenders. The answer contains an advanced project plan, a list of features
and a gant diagram

We chose the project CRISTAL: the junior company of the school. The goal is to redo them a website, all is
detailed in the subject "tender_cristal.pdf".

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build


